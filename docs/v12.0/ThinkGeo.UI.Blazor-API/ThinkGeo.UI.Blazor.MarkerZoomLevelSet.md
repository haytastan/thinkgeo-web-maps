# MarkerZoomLevelSet


## Inheritance Hierarchy

+ `Object`
  + **`MarkerZoomLevelSet`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`MarkerZoomLevelSet()`](#markerzoomlevelset)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`CustomZoomLevels`](#customzoomlevels)|Collection<[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)>|Gets a MarkerZoomLevel collection that is used for you to define your own custom zoomlevels.|
|[`ZoomLevel01`](#zoomlevel01)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level01.|
|[`ZoomLevel02`](#zoomlevel02)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level02.|
|[`ZoomLevel03`](#zoomlevel03)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level03.|
|[`ZoomLevel04`](#zoomlevel04)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level04.|
|[`ZoomLevel05`](#zoomlevel05)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level05.|
|[`ZoomLevel06`](#zoomlevel06)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level06.|
|[`ZoomLevel07`](#zoomlevel07)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level07.|
|[`ZoomLevel08`](#zoomlevel08)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level08.|
|[`ZoomLevel09`](#zoomlevel09)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level09.|
|[`ZoomLevel10`](#zoomlevel10)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level10.|
|[`ZoomLevel11`](#zoomlevel11)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level11.|
|[`ZoomLevel12`](#zoomlevel12)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level12.|
|[`ZoomLevel13`](#zoomlevel13)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level13.|
|[`ZoomLevel14`](#zoomlevel14)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level14.|
|[`ZoomLevel15`](#zoomlevel15)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level15.|
|[`ZoomLevel16`](#zoomlevel16)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level16.|
|[`ZoomLevel17`](#zoomlevel17)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level17.|
|[`ZoomLevel18`](#zoomlevel18)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level18.|
|[`ZoomLevel19`](#zoomlevel19)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level19.|
|[`ZoomLevel20`](#zoomlevel20)|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|Gets the MarkerZoomLevel for Level20.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`GetZoomLevelForDrawing(Int32)`](#getzoomlevelfordrawingint32)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`MarkerZoomLevelSet()`](#markerzoomlevelset)|

### Protected Constructors


### Public Properties

#### `CustomZoomLevels`

**Summary**

   *Gets a MarkerZoomLevel collection that is used for you to define your own custom zoomlevels.*

**Remarks**

   *When you add custom zoomlevels to the CustomZoomLevels collection, the default ZoomLevels 01 through 20 will be disabled. The zoomLevels in the CustomZoomLevels are matched with the scales defined in the Map.ClientZoomLevelScales. For example,CustomZoomLevels[0] has the scale that equals to Map.ClientZoomLevelScales[0].*

**Return Value**

Collection<[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)>

---
#### `ZoomLevel01`

**Summary**

   *Gets the MarkerZoomLevel for Level01.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel02`

**Summary**

   *Gets the MarkerZoomLevel for Level02.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel03`

**Summary**

   *Gets the MarkerZoomLevel for Level03.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel04`

**Summary**

   *Gets the MarkerZoomLevel for Level04.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel05`

**Summary**

   *Gets the MarkerZoomLevel for Level05.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel06`

**Summary**

   *Gets the MarkerZoomLevel for Level06.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel07`

**Summary**

   *Gets the MarkerZoomLevel for Level07.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel08`

**Summary**

   *Gets the MarkerZoomLevel for Level08.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel09`

**Summary**

   *Gets the MarkerZoomLevel for Level09.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel10`

**Summary**

   *Gets the MarkerZoomLevel for Level10.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel11`

**Summary**

   *Gets the MarkerZoomLevel for Level11.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel12`

**Summary**

   *Gets the MarkerZoomLevel for Level12.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel13`

**Summary**

   *Gets the MarkerZoomLevel for Level13.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel14`

**Summary**

   *Gets the MarkerZoomLevel for Level14.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel15`

**Summary**

   *Gets the MarkerZoomLevel for Level15.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel16`

**Summary**

   *Gets the MarkerZoomLevel for Level16.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel17`

**Summary**

   *Gets the MarkerZoomLevel for Level17.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel18`

**Summary**

   *Gets the MarkerZoomLevel for Level18.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel19`

**Summary**

   *Gets the MarkerZoomLevel for Level19.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---
#### `ZoomLevel20`

**Summary**

   *Gets the MarkerZoomLevel for Level20.*

**Remarks**

   *N/A*

**Return Value**

[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetZoomLevelForDrawing(Int32)`

**Summary**

   *Returns the MarkerZoomLevel object whose styles will be applied to the markers based on the current zoomlevel that is passed in.*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|[`MarkerZoomLevel`](ThinkGeo.UI.Blazor.MarkerZoomLevel.md)|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|currentZoomLevelId|`Int32`|An int value that indicates which zoomlevel is the current zoomlevel. The int value must be a number from 1 - 20.|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


